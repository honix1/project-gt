import itertools
import os
from django.shortcuts import render
from django.http import FileResponse, HttpResponse, StreamingHttpResponse

from tools.log_analyzer import LogAnalyzer
from tools.log_loader import LogLoader

STATS_PATH = os.environ['AMS2_SERVER_ROOT'] + r'\lua_config'


def analyze(request):
    log = LogLoader().load()
    log_generator = LogAnalyzer(log).generate_log()
    return StreamingHttpResponse(itertools.chain(['<pre>'], log_generator, ['</pre>']))


def simresults(request):
    return FileResponse(open(STATS_PATH + '\simresults.json', 'rb'))


def rawlog(request):
    return FileResponse(open(STATS_PATH + '\sms_stats_data.json', 'rb'))


def rawlog_get(request, idx):
    result = LogLoader().load().select_event(idx).dumps()

    response = HttpResponse(result, content_type='application/json')
    response['Content-Disposition'] = f'attachment; filename="rawlog_single_event_{idx}.json"'
    return response
