import os
from django.http import FileResponse
from django.urls import path

from . import views

urlpatterns = [
    path('analyze', views.analyze),
    path('rawlog/simresults', views.simresults),
    path('rawlog/rawlog', views.rawlog),
    path('rawlog/rawlog/<int:idx>', views.rawlog_get),
]