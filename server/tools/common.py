import os


STATS_PATH = os.environ['AMS2_SERVER_ROOT'] + \
    r'\lua_config\sms_stats_data.json'

HTTP_SERVER = '127.0.0.1:9000'

def safe_driver_name(name):
    '''Exclude crazy chars from players names'''
    return name.encode('ascii', errors='ignore').decode().strip()
