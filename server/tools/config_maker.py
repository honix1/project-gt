import re


class ConfigMaker:
    """
    ConfigMaker(
        template=r'./server_template.cfg',
        target=r'./server.cfg',
        sub_dict={
            'serverName': 'Enter server name here',
            'password': 'Enter password here',
        }).make_config()

    ConfigMaker(
        template=r'./lua_config/sms_rotate_config_template.json',
        target=r'./lua_config/sms_rotate_config.json',
        sub_dict={
            'trackName': 'Enter track name here',
            'vehicleClassName': 'Enter vehicle class name here',
        }).make_config()
    """

    def __init__(self, template, target, sub_dict):
        self.template = template
        self.target = target
        self.sub_dict = sub_dict

    def make_config(self):
        with open(self.template, 'r') as f:
            data = f.read()

            def repl(matchobj):
                return self.sub_dict[matchobj.group(1)]
            data = re.sub(r'\{\{(.+)\}\}', repl, data)

        with open(self.target, 'w') as f:
            f.write(data)
