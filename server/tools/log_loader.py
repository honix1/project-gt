import json
import re
from tools.common import STATS_PATH

from tools.log_object import RawLog


class LogLoader:
    def __init__(self, raw_log_file_path=STATS_PATH):
        self.raw_log_file_path = raw_log_file_path

    def load(self) -> RawLog:
        with open(self.raw_log_file_path, 'r', encoding='utf-8') as f:
            content = f.read()

        match = re.search(r'{[\s\S]*}', content, re.MULTILINE)
        data = match.group(0)

        # simplify encoding to classics
        data = data.encode('ascii', errors='ignore').decode()

        obj = json.loads(data)

        return RawLog(obj)
