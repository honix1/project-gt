from __future__ import annotations
import json

class RawLog:
    def __init__(self, obj):
        self.obj = obj

    def __getitem__(self, key):
        return self.obj[key]

    def __setitem__(self, key, value):
        self.obj[key] = value

    def dumps(self):
        return json.dumps(self.obj, indent=True)

    def select_event(self, utc_time) -> RawLog:
        self['stats']['history'] = list(
            filter(lambda x: x['start_time'] == utc_time, self['stats']['history']))

        return self
