from http.client import HTTPConnection
import json
from tools.common import HTTP_SERVER


class ApiLoader:
    ''' Load AMS2 server api to get tracks, cars names and other info'''

    def __init__(self, http_server=HTTP_SERVER):
        self.http_server = http_server
        self.list = None

    def get_list(self):
        self.list = self.list or self.__fetch_list()
        return self.list

    def get_tracks_list(self):
        return self.get_list()['tracks']['list']

    # TODO: move to api data object class
    def get_track_name(self, track_id):
        try:
            return next(filter(lambda x: x['id'] == track_id, self.get_tracks_list()))['name']
        except:
            return 'Unable to get track name'

    def __fetch_list(self):
        body = self.__fetch_json_list()
        obj = json.loads(body)

        if obj['result'] != 'ok':
            raise Exception("obj['result'] != 'ok'")

        return obj['response']

    def __fetch_json_list(self):
        con = HTTPConnection(self.http_server)
        con.request('GET', '/api/list') # TODO: switch to static if no server running
        response = con.getresponse()
        body_bytes = response.read()
        result = body_bytes.decode('utf-8', errors='ignore')
        con.close()

        return result


global_api_loader = ApiLoader()
