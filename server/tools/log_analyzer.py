from operator import imod
import time
from typing import Generator
from tools.common import safe_driver_name

from tools.log_object import RawLog
from tools.stage_analyzer import StageAnalyzer
from tools.api_loader import global_api_loader


class LogAnalyzer:
    def __init__(self, raw_log: RawLog):
        self.raw_log = raw_log

    def generate_log(self) -> Generator[str, None, None]:
        obj = self.raw_log.obj

        stats = obj['stats']
        history = stats['history']
        players = stats['players']

        yield '\n'
        yield f'{"Project-gt AMS2 server statistics":^135}\n'
        yield '\n'

        yield 'Players race joints:\n'
        yield '\n'
        for player in players.values():
            name = safe_driver_name(player['name'])
            race_joints = player['counts']['race_joins']
            yield f'{name:>30} {race_joints:>10}\n'

        yield '\n'
        yield '=' * 135 + '\n'

        yield '\n'
        for event in history:
            yield from LogAnalyzer.__generate_event_log(event)

    def __generate_event_log(event_obj):
        finished = event_obj['finished']

        if not finished:
            return

        utc_start_time_seconds = event_obj['start_time']
        start_time = time.gmtime(utc_start_time_seconds)
        setup = event_obj['setup']
        track_id = setup['TrackId']
        track_name = global_api_loader.get_track_name(track_id)

        yield 'Event finished:\n'
        yield '\n'
        yield time.strftime('Time: %a, %d %b %Y %H:%M:%S UTC\n', start_time)
        yield f'Track: {track_name}\n'
        yield '\n'
        yield f'<a href="/service/rawlog/rawlog/{utc_start_time_seconds}">Download rawlog of this event</a>\n'
        yield '\n'

        members = event_obj['members']
        stages = event_obj['stages']
        for stage in stages:
            stage_analyzer = StageAnalyzer(members, stage, stages[stage])
            yield from stage_analyzer.generate_stage_log()

        yield '=' * 135 + '\n'
