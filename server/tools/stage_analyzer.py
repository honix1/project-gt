import sys
import time
from typing import Generator

from tools.common import safe_driver_name


class StageAnalyzer:
    def __init__(self, members, name, stage):
        self.members = members
        self.name = name
        self.stage = stage

        self.best_total_laps = None
        self.best_total_time_ms = None
        self.best_lap_time_ms = sys.maxsize

    def generate_stage_log(self) -> Generator[str, None, None]:
        stage_name = self.name
        is_race = 'race' in stage_name

        yield f'Stage {stage_name}:\n'
        yield '\n'

        if not is_race:
            yield (
                f'{"Driver":>30}'
                f',{"Sector 1":>20}'
                f',{"Sector 2":>20}'
                f',{"Sector 3":>20}'
                f',{"Fastest lap time":>20}'
                f',{"Gap":>20}\n'
            )
        else:
            yield (
                f'{"Driver":>30}'
                f',{"Total time":>20}'
                f',{"Gap":>20}'
                f',{"Fastest lap time":>20}'
                f',{"Gap":>20}'
                f',{"Laps":>20}\n'
            )
        yield '\n'

        data = self.stage
        events = data['events']
        lap_events = list(
            filter(lambda x: x['event_name'] == 'Lap', events))
        results = data['results']

        # TODO: refactor to lists
        # upd: actually it needs a objective pass
        for result in results:
            attributes = result['attributes']
            lap = attributes['Lap']
            total_time_ms = attributes['TotalTime']
            fastest_lap_time_ms = attributes['FastestLapTime']
            if not self.best_total_laps:
                self.best_total_laps = lap
            if not self.best_total_time_ms:
                self.best_total_time_ms = total_time_ms
            if fastest_lap_time_ms and fastest_lap_time_ms < self.best_lap_time_ms:
                self.best_lap_time_ms = fastest_lap_time_ms

        # if best_total_time_ms == sys.maxsize:
        #     best_total_time_ms = None
        # if best_lap_time_ms == sys.maxsize:
        #     best_lap_time_ms = None

        for result in results:
            yield from self.__generate_result_log(
                result, is_race, lap_events)

        yield '\n'

    def __driver_refid_to_name(self, refid):
        return self.members[str(refid)]['name']

    def __format_ms_time(time_ms):
        total_time = time.gmtime(time_ms / 1000)
        time_ms_part = time_ms % 1000
        total_time_str = time.strftime(
            f'%H:%M:%S.{time_ms_part:03d}', total_time)
        return total_time_str

    def __format_race_time(time_ms):
        if time_ms == 0:
            total_time_str = 'DNF'
        else:
            total_time_str = StageAnalyzer.__format_ms_time(time_ms)
        return total_time_str

    def __format_gap_time(time_ms_from, time_ms_to, lap_from=0, lap_to=0):
        if lap_from > lap_to:
            return f'{lap_from-lap_to} Laps'
        if not time_ms_from or not time_ms_to:
            return 'No data'
        time_ms = time_ms_to - time_ms_from
        # if time_ms > 0:
        #     sign = '+'
        # elif time_ms < 0:
        #     sign = '-'
        # else:
        #     sign = '='
        # return f'{sign} {format_race_time(time_ms)}'
        return StageAnalyzer.__format_ms_time(time_ms)

    def __generate_result_log(self, result_obj, is_race, lap_events):
        best_total_laps = self.best_total_laps
        best_total_time_ms = self.best_total_time_ms
        best_lap_time_ms = self.best_lap_time_ms

        driver_refid = result_obj['refid']
        driver_name_raw = result_obj['name'] or self.__driver_refid_to_name(
            driver_refid)
        driver_name = safe_driver_name(driver_name_raw)
        attributes = result_obj['attributes']
        lap = attributes['Lap']
        total_time_ms = attributes['TotalTime']
        fastest_lap_time_ms = attributes['FastestLapTime']
        finished = attributes['State'] == 'Finished'
        fastest_lap_event = \
            next(
                filter(
                    lambda x:
                        x['refid'] == driver_refid and
                        x['attributes']['LapTime'] == fastest_lap_time_ms, lap_events),
                None) \
            # or \
        # next(
        #     iter(
        #         sorted(
        #             filter(
        #                 # sometimes this necessery
        #                 lambda x:
        #                     x['attributes']['CountThisLapTimes'] == 1 and
        #                     x['name'] == driver_name_raw,
        #                 lap_events),
        #             key=lambda x: x['attributes']['LapTime'])),
        #     None)

        if fastest_lap_event:
            fastest_lap_attributes = fastest_lap_event['attributes']
            sector1 = fastest_lap_attributes['Sector1Time']
            sector2 = fastest_lap_attributes['Sector2Time']
            sector3 = fastest_lap_attributes['Sector3Time']
        else:
            sector1 = 0
            sector2 = 0
            sector3 = 0
            if not is_race:
                fastest_lap_time_ms = 0  # this lap was invalid
        fastest_lap_time_str = StageAnalyzer.__format_race_time(
            fastest_lap_time_ms)
        fastest_lap_time_gap_str = StageAnalyzer.__format_gap_time(
            best_lap_time_ms, fastest_lap_time_ms)

        if not is_race:
            yield (
                f'{driver_name:>30}'
                f',{StageAnalyzer.__format_race_time(sector1):>20}'
                f',{StageAnalyzer.__format_race_time(sector2):>20}'
                f',{StageAnalyzer.__format_race_time(sector3):>20}'
                f',{fastest_lap_time_str:>20}'
                f',{fastest_lap_time_gap_str:>20}\n'
            )
        else:
            total_gap = StageAnalyzer.__format_gap_time(
                best_total_time_ms, total_time_ms,
                best_total_laps, lap)
            total_time = StageAnalyzer.__format_race_time(
                total_time_ms) if finished else 'Retired'
            yield (
                f'{driver_name:>30}'
                f',{total_time:>20}'
                f',{total_gap:>20}'
                f',{fastest_lap_time_str:>20}'
                f',{fastest_lap_time_gap_str:>20}'
                f',{lap:>20}\n'
            )
